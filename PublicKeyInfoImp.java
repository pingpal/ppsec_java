package io.pingpal.crypto;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public class PublicKeyInfoImp implements PublicKeyInfo {

    byte[] publicCrypotKey;
    String issuer;
    String client;
    byte [] signature;

    public PublicKeyInfoImp(byte[] publicCrypotKey, String issuer, String client, byte[] signature){
        this.publicCrypotKey = publicCrypotKey;
        this.issuer = issuer;
        this.client = client;
        this.signature = signature;
    }

    @Override
    public byte[] getPublicCryptoKey() {
        return publicCrypotKey;
    }

    @Override
    public String getIssuerCertificate() {
        return issuer;
    }

    @Override
    public String getClientCertificate() {
        return client;
    }

    @Override
    public byte[] getSignature() {
        return signature;
    }
}
