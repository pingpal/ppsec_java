package io.pingpal.crypto;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.Map;

public class MyActivity extends Activity{


    static {

        System.loadLibrary("kaliumjni");

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my);

        dsa();
    }


    public void dsa(){

        final String t1 = "thisistag"+Math.random();
        final String t2 = "thisistag"+Math.random();

        final PPCrypto c1 =  new PPCrypto(t1,"https://pki.pngpl.com/api/api.",null,null,null,"io.pingpal.crypto",getBaseContext());
        final PPCrypto c2 =  new PPCrypto(t2,"https://pki.pngpl.com/api/api.",null,null,null,"io.pingpal.crypto2",getBaseContext());

        c1.registerUser(new RegisterCallback() {
            @Override
            public void call(Throwable e) {
                Log.d("JakobTag","firstreg");
                if (e != null){
                    e.printStackTrace();
                }else {
                    c2.registerUser(new RegisterCallback() {
                        @Override
                        public void call(Throwable e) {
                            Log.d("JakobTag","secondreg");
                            if (e != null){
                                e.printStackTrace();
                            }else {


                                Map<String,Object> m = new HashMap<String, Object>();

                                m.put("m","message");

                                c1.encrypt(m,t2,new CryptoCallback() {
                                    @Override
                                    public void call(Throwable e, Map<String, Object> map) {

                                        if (e != null){
                                            e.printStackTrace();
                                        }else {

                                            for (String key : map.keySet()){
                                                Log.d("JakobTag",key + map.get(key));
                                            }


                                            c2.decrypt(map,new CryptoCallback() {
                                                @Override
                                                public void call(Throwable e, Map<String, Object> map) {
                                                    if (e != null){
                                                        e.printStackTrace();
                                                    }else {

                                                        Log.d("JakobTag","success:"+map.get("m"));

                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
