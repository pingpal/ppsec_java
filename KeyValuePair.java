package io.pingpal.crypto;

import java.util.Comparator;

/**
 * Created by joakim on 24/03/15.
 */
public class KeyValuePair {

    String key;
    String value;

    public KeyValuePair(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey(){
        return key;
    }

    public String getValue(){
        return value;
    }
}