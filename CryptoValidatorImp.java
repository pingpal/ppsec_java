package io.pingpal.crypto;

import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public class CryptoValidatorImp implements CryptoValidator {


    @Override
    public void validateChain(List<X509Certificate> chain) throws CertificateException{

        Iterator<X509Certificate> itr = chain.iterator();

        X509Certificate dad = itr.next();
        dad.checkValidity();

        while (itr.hasNext()){
            X509Certificate child = itr.next();
            child.checkValidity();
            try {
                child.verify(dad.getPublicKey());
            } catch (Exception e) {
                throw new CertificateException(e);
            }
            dad = child;
        }
    }

    @Override
    public void validateSignature(X509Certificate cert, byte[] data, byte[] signature) throws CertificateException{

        try{
        Signature signVerifier = Signature.getInstance("sha1WithRSA");
        signVerifier.initVerify(cert);
        signVerifier.update(data);
        signVerifier.verify(signature);
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    @Override
    public void validateExcluded(X509Certificate issuerCert, List<X509Certificate> revoked) throws CertificateException {

        for (X509Certificate revCert : revoked){
            if (revCert.equals(issuerCert)){
                throw new CertificateException("The issuer of the clients certificate has been revoked, certificate is compromised");
            }
        }

    }
}
