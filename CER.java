package io.pingpal.crypto;

import android.content.Context;
import android.util.Base64;

import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.OperatorCreationException;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.pkcs.PKCS10CertificationRequest;
import org.spongycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.spongycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyPair;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

/**
 * Created by jakobfolkesson on 2014-08-25.
 */

public class CER {

    public static String generateCSR(String tag, KeyPair pair){



        PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
                new X500Principal("CN="+tag), pair.getPublic());
        JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA1withRSA");
        ContentSigner signer = null;

        try {
            signer = csBuilder.build(pair.getPrivate());
        } catch (OperatorCreationException e) {
            e.printStackTrace();
        }

        PKCS10CertificationRequest csr = p10Builder.build(signer);

        byte [] s = null;

        try {
            s = csr.toASN1Structure().getEncoded();
           // s = csr.getEncoded();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String string = new String( android.util.Base64.encode(s, Base64.DEFAULT) );

        string = "-----BEGIN CERTIFICATE REQUEST-----\n" + string + "-----END CERTIFICATE REQUEST-----";


        return string;
    }


    public static X509Certificate getRootCA(Context context) throws CertificateException{

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = context.getResources().openRawResource(R.raw.root_ca);
        X509Certificate ca;
        try {
            ca = (X509Certificate) cf.generateCertificate(caInput);
        } finally {
            closeStream(caInput);
        }
        return ca;
    }

    private static void closeStream(InputStream is){
        try {
            is.close();
        } catch (IOException e) {}
    }


    public static byte[] sign(byte [] toSign, KeyPair pair) throws IOException{

        JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA1withRSA");
        ContentSigner signer = null;

        try {
            signer = csBuilder.build(pair.getPrivate());
        } catch (OperatorCreationException e) {

            e.printStackTrace();
        }
        OutputStream os =  signer.getOutputStream();

        os.write(toSign);

        return signer.getSignature();
    }


    public static X509Certificate createCertificate(String base64Cert) throws CertificateException{

        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        InputStream in = new ByteArrayInputStream(base64Cert.getBytes());
        X509Certificate cert = (X509Certificate)certFactory.generateCertificate(in);

        return cert;

    }

}
