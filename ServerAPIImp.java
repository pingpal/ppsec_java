package io.pingpal.crypto;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
//import java.util.Base64;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public class ServerAPIImp implements ServerAPI {

    private String baseURL;
    //private Context context;


    public ServerAPIImp(String baseURL/*,Context context*/){
        this.baseURL = baseURL;
        //TODO
        //this.context = context;
    }


    @Override
    public void registerWithCSR(String csr) throws APICallException {

        List<KeyValuePair> args = new ArrayList<KeyValuePair>();
        args.add(new KeyValuePair("csr",csr));

        try {
            new APICall(args,baseURL+"addusr"/* ,context*/).go();
        }catch (IOException e){
            throw new APICallException(e);
        }


    }

    @Override
    public void putCryptoKey(String tag, String pubCryptoKey64, String sign64) throws APICallException {

        List<KeyValuePair> args = new ArrayList<KeyValuePair>();

        //Log.d("puttingCryptKey",pubCryptoKey64);

        args.add(new KeyValuePair("uid",tag));
        args.add(new KeyValuePair("cryptkey",pubCryptoKey64));
        args.add(new KeyValuePair("cryptkeysign",sign64));

        try {
            JSONObject object = new APICall(args,baseURL+"addenc").go();

            if (!object.getString("result").equals("success")){
                throw new APICallException("BadServerResponse:"+object.toString());
            }

        } catch (IOException e) {
            throw new APICallException(e);
        }catch (JSONException e){
            throw new APICallException(e);
        }


    }

    @Override
    public PublicKeyInfo getPublicKey(String recipient) throws APICallException {

        List<KeyValuePair> args = new ArrayList<KeyValuePair>();
        args.add(new KeyValuePair("uid",recipient));

        try {
            JSONObject object = new APICall(args,baseURL+"getcrypt").go();

            object = object.getJSONObject("response");

            String cert = object.getString("cert");
            String issuer = object.getString("issuer");
            String signString = object.getString("cryptkeysign");
            String pubCryptKeyString = object.getString("cryptkey");

            //Log.d("JakobTag","cryptoKey:"+pubCryptKeyString);
            //TODO nowrap?
           // byte [] sign = Base64.decode(signString,Base64.NO_WRAP);
           // byte [] pubCryptKey = Base64.decode(pubCryptKeyString,Base64.NO_WRAP);
            byte[] sign = Base64.decodeBase64(signString);
            byte [] pubCryptKey = Base64.decodeBase64(pubCryptKeyString);
            return new PublicKeyInfoImp(pubCryptKey,issuer,cert,sign);

        } catch (IOException e) {
            throw new APICallException(e);
        }catch (JSONException e){
            throw new APICallException(e);
        }

    }

    @Override
    public List<X509Certificate> getRevokedIssuers() throws APICallException {

        try {

            List<KeyValuePair> l = new ArrayList<KeyValuePair>();

            JSONObject res = new APICall(l, baseURL+"getrevoked").go();

            JSONArray revokedJSON = res.getJSONArray("response");
            List<X509Certificate> certs = new ArrayList<X509Certificate>(revokedJSON.length());

            for (int i = 0; i < revokedJSON.length(); i++) {
                certs.add(CER.createCertificate(revokedJSON.getString(i)));
            }

            return certs;

        }catch (IOException e){
            throw new APICallException(e);
        }catch (JSONException e){
            throw new APICallException(e);
        }catch (CertificateException e){
            throw new APICallException(e);
        }
    }
}
