package io.pingpal.crypto;

import java.util.Map;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public interface CryptoCallback {

    public void call(Throwable e,Map<String,Object> map);

}
