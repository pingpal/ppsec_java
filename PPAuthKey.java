package io.pingpal.crypto;

import org.spongycastle.jcajce.provider.asymmetric.rsa.KeyPairGeneratorSpi;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.spec.RSAKeyGenParameterSpec;

/**
 * Created by jakobfolkesson on 2014-08-22.
 */
public class PPAuthKey {

    final static int KEY_SIZE = 2048;

    public static KeyPair generate(){

        SecureRandom random = new SecureRandom();
        RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(KEY_SIZE, RSAKeyGenParameterSpec.F4);
        KeyPairGenerator generator = new KeyPairGeneratorSpi();
        try {
            generator.initialize(spec, random);
        } catch (InvalidAlgorithmParameterException e) {
            return null;
        }

        return generator.generateKeyPair();
    }

    public void dsa(){



    }
}
