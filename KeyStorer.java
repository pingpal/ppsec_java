package io.pingpal.crypto;

import java.security.KeyPair;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public class KeyStorer {

    private org.abstractj.kalium.keys.KeyPair cryptoKeys;
    private KeyPair authKeys;

    //TODO
    public KeyStorer(/*Context context,*/ String keyStorePath) {

        cryptoKeys = new org.abstractj.kalium.keys.KeyPair();
        authKeys = PPAuthKey.generate();
    }

    public boolean hasAutyKeys(){
        return true;
    }

    public void storeAuthKeys(KeyPair keyPair){}

    public KeyPair getAuthKeys(){
        return authKeys;
    }

    public boolean hasCryptoKeys(){
        return true;
    }

    public void storeCryptoKeys(org.abstractj.kalium.keys.KeyPair keyPair){}

    public org.abstractj.kalium.keys.KeyPair getCryptoKeys(){
        return cryptoKeys;
    }

}
