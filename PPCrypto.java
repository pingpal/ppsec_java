package io.pingpal.crypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.abstractj.kalium.SodiumConstants;
import org.abstractj.kalium.crypto.Box;
import org.abstractj.kalium.crypto.Random;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.KeyPair;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public class PPCrypto {

    private String tag;
    private String rootCertName;
    private String devKeyPath;
    //TODO
    // private Context context;
    private String prefPath;
    private ServerAPI serverAPI;
    private KeyStorer ks;
    private CryptoValidator validator;

    public PPCrypto(String tag, String baseURL, String rootCertName, String devKeyPath,String keyStorePath, String prefPath /*Context context*/){

        this.tag = tag;
        this.rootCertName = rootCertName;
        this.devKeyPath = devKeyPath;
        //TODO
        //this.context = context;
        this.prefPath = prefPath;
        ks = new KeyStorer(keyStorePath);

        serverAPI = new ServerAPIImp(baseURL);
        validator = new CryptoValidatorImp();

    }


    public void registerUser(final RegisterCallback callback){

        checkFirst();

        new AsyncTask<Void,Void,Throwable>(){

            @Override
            protected Throwable doInBackground(Void... voids) {

                Throwable error = null;
                try {
                    PPCrypto.this.registerUserSync();
                }catch (Exception e){
                    error = e;
                }
                return error;
            }
            @Override
            protected void onPostExecute(Throwable error){
                callback.call(error);
            }

        }.execute();
    }

    public void encrypt(final Map<String, Object> payload,final String recipient, final CryptoCallback callback){

        new AsyncTask<Void,Void,Object[]>(){

            @Override
            protected Object[] doInBackground(Void... voids) {
                Map<String,Object> map = null;
                Throwable error = null;
                try {
                    map = encryptSync(payload, recipient);
                }catch (Exception e){
                    error = e;
                }

                return new Object[]{error,map};
            }

            @Override
            protected void onPostExecute(Object[] objects){

                Throwable error = (Throwable)objects[0];
                Map<String,Object> map = (Map<String,Object>)objects[1];

                callback.call(error,map);
            }

        }.execute();
    }

    public void decrypt(final Map<String, Object> safeMap, final CryptoCallback callback){

        new AsyncTask<Void,Void,Object[]>(){

            @Override
            protected Object[] doInBackground(Void... voids) {
                Map<String,Object> map = null;
                Throwable error = null;
                try {
                    map = decryptSync(safeMap);
                }catch (Exception e){
                    error = e;
                }
                return new Object[]{error,map};
            }

            @Override
            protected void onPostExecute(Object[] objects){

                Throwable error = (Throwable)objects[0];
                Map<String,Object> map = (Map<String,Object>)objects[1];

                callback.call(error,map);
            }
        }.execute();
    }


    public void registerUserSync() throws APICallException{

        checkFirst();

        if (!ks.hasAutyKeys()){
            ks.storeAuthKeys(PPAuthKey.generate());
        }

        if (!ks.hasCryptoKeys()){
            ks.storeCryptoKeys(new org.abstractj.kalium.keys.KeyPair());
        }

        KeyPair authKeys = ks.getAuthKeys();
        org.abstractj.kalium.keys.KeyPair cryptoKeys = ks.getCryptoKeys();

        byte [] sign = null;
        try{
            sign = CER.sign(cryptoKeys.getPublicKey().toBytes(),authKeys);
        }catch (IOException e){
            throw new APICallException("Error while signing keys");
        }
        String sign64 = Base64.encodeToString(sign,Base64.NO_WRAP);
        String cryptoKey64 = Base64.encodeToString(cryptoKeys.getPublicKey().toBytes(),Base64.NO_WRAP);

        String certificate = CER.generateCSR(tag,authKeys);
        serverAPI.registerWithCSR(certificate);
        serverAPI.putCryptoKey(tag,cryptoKey64,sign64);

        SharedPreferences prefs = context.getSharedPreferences(prefPath,Context.MODE_PRIVATE);

        prefs.edit().putBoolean(prefPath + ".has.registered",true).commit();

    }

    private void checkFirst(){
        if (true)   return;
        SharedPreferences prefs = context.getSharedPreferences(prefPath,Context.MODE_PRIVATE);

        boolean hasRegisterd = prefs.getBoolean(prefPath + ".has.registered",false);

        if (hasRegisterd){
            throw new RuntimeException("You have already registered");
        }
    }

    public Map<String,Object> encryptSync(Map<String, Object> payload, String recipient) throws APICallException, CertificateException{

        if (recipient == null || recipient.length() <1){
            throw new RuntimeException("Invalid recipient");
        }

        byte [] yourPublicKey = fetchPublicKey(recipient);
        byte [] message = new JSONObject(payload).toString().getBytes();
        byte [] myprivateKey = ks.getCryptoKeys().getPrivateKey().toBytes();

        Box outBox = new Box(yourPublicKey,myprivateKey);

        byte [] nonce = (new Random()).randomBytes(SodiumConstants.NONCE_BYTES);
        byte [] cipherBytes = outBox.encrypt(nonce,message);

        String chifferString = Base64.encodeToString(cipherBytes,Base64.NO_WRAP);
        String nonceString = Base64.encodeToString(nonce,Base64.NO_WRAP);

        Map<String,Object> encryptedMessage = new HashMap<String, Object>();

        encryptedMessage.put("c_cipher",chifferString);
        encryptedMessage.put("c_nonce",nonceString);
        encryptedMessage.put("c_uid",tag);
        encryptedMessage.put("c_version",0.1);

        return encryptedMessage;
    }

    public Map<String,Object> decryptSync(Map<String, Object> safeMap) throws APICallException, CertificateException, JSONException{

        String nonceString = (String) safeMap.get("c_nonce");
        byte [] nonce = Base64.decode(nonceString,Base64.NO_WRAP);

        String cipherString = (String) safeMap.get("c_cipher");
        byte [] chipher = Base64.decode(cipherString,Base64.NO_WRAP);

        String recipient = (String) safeMap.get("c_uid");

        byte[] yourPublicKey = fetchPublicKey(recipient);
        byte[] myprivateKey = ks.getCryptoKeys().getPrivateKey().toBytes();
        Box outBox = new Box(yourPublicKey,myprivateKey);

        byte [] message = outBox.decrypt(nonce,chipher);
        String messageString = new String(message);

        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String,Object> payload = gson.fromJson(messageString, stringStringMap);

        return payload;

    }



    private byte[] fetchPublicKey(String tag)  throws APICallException, CertificateException{

        PublicKeyInfo keyInfo =  serverAPI.getPublicKey(tag);

        X509Certificate rootCertificate = CER.getRootCA(context);
        X509Certificate issuerCert = CER.createCertificate(keyInfo.getIssuerCertificate());
        X509Certificate clientCert = CER.createCertificate(keyInfo.getClientCertificate());

        List<X509Certificate> certificateChain = new ArrayList<X509Certificate>();

        certificateChain.add(rootCertificate);
        certificateChain.add(issuerCert);
        certificateChain.add(clientCert);

        validator.validateChain(certificateChain);

        List<X509Certificate> revoked = serverAPI.getRevokedIssuers();
        validator.validateExcluded(issuerCert,revoked);

        byte [] yourpub = keyInfo.getPublicCryptoKey();

        validator.validateSignature(clientCert,yourpub,keyInfo.getSignature());

        return yourpub;
    }

}
