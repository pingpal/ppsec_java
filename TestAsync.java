package io.pingpal.crypto;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.abstractj.kalium.keys.PublicKey;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyPair;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jakobfolkesson on 2014-08-28.
 */
public class TestAsync extends AsyncTask<Void, Void, Void> {

    private Context context;

    public TestAsync(Context context){

        this.context = context;

    }

    @Override
    protected Void doInBackground(Void... voids) {

        try {

            final String tag = "jakobmobob212121";
            final KeyPair authKeyPair = PPAuthKey.generate();
            final org.abstractj.kalium.keys.KeyPair cryptoKey = new org.abstractj.kalium.keys.KeyPair();
            final PublicKey mypub = cryptoKey.getPublicKey();
            final byte[] publicCryptoKey = mypub.toBytes();


            //JSONObject obj = registerUser(tag,authKeyPair);

            //Log.d("JakobTag","regUsr:"+obj);

            //if (true)  return null;
/*
                if (![val validate:pubCryptKey :sign :cert :issuer]) {
                    *err = [NSError errorWithDomain:@"Error validating public crypto key." code:-1 userInfo:nil];
                    return @"";
                }

                return pubCryptKey;
                ///////*/

            JSONObject object = getPublicEncryptionKey(tag);

            String cert = null;
            String issuer = null;
            String sign = null;
            String pubCryptKey = null;


            try {
                object = object.getJSONObject("response");

                cert = object.getString("cert");
                issuer = object.getString("issuer");
                sign = object.getString("cryptkeysign");
                pubCryptKey = object.getString("cryptkey");
            } catch (JSONException e) {
                throw new APICallException("Data returned from server is all wrong");
            }

            byte [] byteSignature = Base64.decode(sign,Base64.DEFAULT);
            X509Certificate rootCert = CER.getRootCA(context);
            X509Certificate issuerCert = CER.createCertificate(issuer);
            X509Certificate clientCert = CER.createCertificate(cert);
            List<String> revoked = getRevoked();

            for (String revString : revoked){
               Certificate revCert = CER.createCertificate(revString);

                if (revCert.equals(issuerCert)){
                    throw new APICallException("The issuer of the clients certificate has been revoked, certificate is compromised");
                }
            }

            rootCert.checkValidity();
            issuerCert.checkValidity();
            clientCert.checkValidity();

            Signature signVerifier = Signature.getInstance("sha1WithRSA");
            signVerifier.initVerify(clientCert);
            signVerifier.update(Base64.decode(pubCryptKey,Base64.DEFAULT));

            clientCert.verify(issuerCert.getPublicKey());
            issuerCert.verify(rootCert.getPublicKey());
            signVerifier.verify(byteSignature);

            //registerUser(tag,authKeyPair,registerUserCallback);
            //getRevoked(revokedKeysCallback);

        }catch (Exception e){

            throw new AssertionError(e);
        }
        return null;
    }



    public JSONObject registerUser(String tag ,KeyPair keyPair) throws IOException {

        List<KeyValuePair> args = new ArrayList<KeyValuePair>();

        String csr = CER.generateCSR(tag,keyPair);

        Log.d("JakobTag",csr);

        args.add(new KeyValuePair("csr",csr));

        return new APICall(args,"addusr",context).go();

    }

    public JSONObject registerPublicCryptoKey(String tag, KeyPair authKey, byte[] cryptoKey) throws IOException{

        byte [] signature = null;

        try {
            signature = CER.sign(cryptoKey, authKey);
        }catch (IOException e){
            return new JSONObject();
        }

        String signature64 = new String(Base64.encode(signature, Base64.NO_WRAP));
        String cryptoKey64 = new String(Base64.encode(cryptoKey,Base64.NO_WRAP));

        Log.d("JakobTag", "sign:" + signature64 + "|cryptoKey:" + cryptoKey64);

        List<KeyValuePair> args = new ArrayList<NameValuePair>();

        args.add(new KeyValuePair("uid",tag));
        args.add(new KeyValuePair("cryptkey",cryptoKey64));
        args.add(new KeyValuePair("cryptkeysign",signature64));

        return new APICall(args,"addenc",context).go();


    }

    public List<String> getRevoked() throws APICallException {

        try {

            List<KeyValuePair> l = new ArrayList<KeyValuePair>();

            JSONObject res = new APICall(l, "getrevoked", context).go();

            JSONArray revokedJSON = res.getJSONArray("response");
            List<String> certs = new ArrayList<String>(revokedJSON.length());

            for (int i = 0; i < revokedJSON.length(); i++) {
                certs.add(revokedJSON.getString(i));
            }

        return certs;

        }catch (IOException e){
            throw new APICallException(e);
        }catch (JSONException e){
            throw new APICallException(e);
        }
    }


    public JSONObject getPublicEncryptionKey(String tag) throws IOException{

        List<KeyValuePair> args = new ArrayList<KeyValuePair>();
        args.add(new KeyValuePair("uid",tag));

        return new APICall(args,"getcrypt",context).go();

    }
}
