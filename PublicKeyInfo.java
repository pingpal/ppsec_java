package io.pingpal.crypto;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public interface PublicKeyInfo {

    public byte[] getPublicCryptoKey();
    public String getIssuerCertificate();
    public String getClientCertificate();
    byte[] getSignature();
}
