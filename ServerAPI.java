package io.pingpal.crypto;

import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public interface ServerAPI {

    public void registerWithCSR(String csr) throws APICallException;

    public void putCryptoKey(String tag,String pubCryptoKey64, String sign64) throws APICallException;

    public PublicKeyInfo getPublicKey(String recipient) throws APICallException;

    public List<X509Certificate> getRevokedIssuers() throws APICallException;
}
