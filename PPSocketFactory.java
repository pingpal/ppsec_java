package io.pingpal.crypto;

import android.content.Context;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by jakobfolkesson on 2014-08-26.
 */

public class PPSocketFactory {



    public SSLSocketFactory createSocketFactory() {
        try {

            TrustManager[] tm = getTrustManager();

            KeyManagerFactory kmf =  getKeyManagerFactory();

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tm, new SecureRandom());
            javax.net.ssl.SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return sslSocketFactory;

        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }


    private TrustManager[] getTrustManager() throws Exception{

        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);
        trustStore.setCertificateEntry("ca", CER.getRootCA(context));

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(trustStore);

        return tmf.getTrustManagers();

    }


    private KeyManagerFactory getKeyManagerFactory() throws Exception{
        InputStream keyStoreStream = context.getResources().openRawResource(R.raw.pki_access);
        KeyStore keyStore = null;
        keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(keyStoreStream, "123456789".toCharArray());

        KeyManagerFactory keyManagerFactory = null;
        keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, "123456789".toCharArray());

        return keyManagerFactory;
    }
}