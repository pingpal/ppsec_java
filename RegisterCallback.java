package io.pingpal.crypto;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public interface RegisterCallback {

    public void call(Throwable e);
}
