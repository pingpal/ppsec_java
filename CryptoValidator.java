package io.pingpal.crypto;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Created by jakobfolkesson on 2014-08-29.
 */
public interface CryptoValidator {

    public void validateChain(List<X509Certificate> chain)  throws CertificateException;
    public void validateSignature(X509Certificate cert, byte [] data, byte [] signature)  throws CertificateException;
    void validateExcluded(X509Certificate issuerCert, List<X509Certificate> revoked)  throws CertificateException;
}
