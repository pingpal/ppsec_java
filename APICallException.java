package io.pingpal.crypto;

/**
 * Created by jakobfolkesson on 2014-08-28.
 */
public class APICallException extends Exception{

    public APICallException(Exception e){

        super(e);

    }

    public APICallException(String message){

        super(message);

    }

}
