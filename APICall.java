package io.pingpal.crypto;


import org.apache.http.NameValuePair;
import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by jakobfolkesson on 2014-08-28.
 */

public class APICall {

    private URL siteUrl;
    //private Context context;

    List<KeyValuePair> params;

    //TODO
    public APICall(List<KeyValuePair> params, String url/*, Context context*/  ){
        try {
            siteUrl = new URL(url);
        }catch (MalformedURLException e){
            throw new AssertionError(e);
        }
        this.params = params;
        //this.context = context;
    }


    public JSONObject go() throws IOException{

        HttpsURLConnection urlConnection = null;

        urlConnection = (HttpsURLConnection) siteUrl.openConnection();
        urlConnection.setSSLSocketFactory(new PPSocketFactory().createSocketFactory());
        urlConnection.setHostnameVerifier(new StrictHostnameVerifier());


        urlConnection.setReadTimeout(10000);
        urlConnection.setConnectTimeout(15000);
        try {
            urlConnection.setRequestMethod("POST");
        }catch (ProtocolException e){
            throw new AssertionError(e);
        }
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);

        OutputStream os = urlConnection.getOutputStream();

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.write(getQuery(params));
        writer.flush();
        writer.close();
        os.close();

        urlConnection.connect();
        InputStream is = null;

        if ( urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
            is = urlConnection.getInputStream();
        }else{
            is = urlConnection.getErrorStream();
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        String s = new String(buffer.toByteArray());
        buffer.close();

        JSONObject object = new JSONObject();
        try {
            object.put("code",1);
            object.put("result","failure");
            object = new JSONObject(s);
        } catch (JSONException e) {}

        return object;
    }

    private String getQuery(List<KeyValuePair> params){
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (KeyValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            String encKey = null;
            String encValue = null;
            try {
                encKey = URLEncoder.encode(pair.getKey(), "UTF-8");
                encValue = URLEncoder.encode(pair.getValue(), "UTF-8");
            } catch (UnsupportedEncodingException e) {}

            result.append(encKey);
            result.append("=");
            result.append(encValue);
        }

        return result.toString();
    }
}